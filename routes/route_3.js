var express = require('express');
var router = express.Router();
var Op = require('../models/OperatorModel')

router.get('/', function(req, res, next) {
   Op.find((err, data) => {
        res.render('view2', {
      data:data
    });
    })
   
});

router.get('/add', function(req, res, next) {
   res.render('add_Db');
});

router.post('/add', (req, res) => {
  var o = new Op( {
      name: req.body.name,
      tactic: req.body.tactic,
      link: req.body.link,
      img: req.body.img,
   }).save();
   
   res.redirect("/database");
});

module.exports = router;