var express = require('express');
var router = express.Router();
const fs = require('fs');
var path = require('path')

var j = fs.readFileSync(path.join(__dirname, '/data/data.json'), 'utf8')
var data = JSON.parse(j);

router.get('/', function(req, res, next) {
   res.render('view1', {
      data: data
    });
});

router.get('/add', function(req, res, next) {
   res.render('add_Json');
});


router.post('/add', (req, res) => {
  var o = {
      name: req.body.name,
      tactic: req.body.tactic,
      link: req.body.link,
      img: req.body.img
   }
   data.unshift(o)
   fs.writeFile(path.join(__dirname, '/data/data.json'), JSON.stringify(data, null, 2), (err) => {
      if (err) {
         console.log(err);
      }
   })

   res.redirect("/json-load");
});

module.exports = router;