var mongoose = require('mongoose');

var OperatorsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    tactic: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    link: {
        type: String,
        required: true
    }
},{collection : 'Operator'});

module.exports = mongoose.model('Operators', OperatorsSchema);