const app = require('../app')
const request = require('supertest')
const chai = require('chai'),
    expect = chai.expect , assert = chai.assert;

 var o = {
    name: "testname",
    tactic: "test",
    link: "https://www.ubisoft.com/en-us/game/rainbow-six/siege/game-info/operators/jackal",
    img:"./images/Hibana.png",
   }

describe('JSON', () => {
    it('Render View with data', done => {
        request(app)
            .get('/json-load')
            .end((err, res) => {
                 expect(res.header['content-type']).to.equal('text/html; charset=utf-8');
                 expect(res.text).to.include("Warden")
                 done()          
            })
              
    })
     it('Render View inputs', done => {
        request(app)
            .get('/json-load/add')
            .end((err, res) => {
                 expect(res.header['content-type']).to.equal('text/html; charset=utf-8');
                 expect(res.text).to.include("Add Operators")
                 done()          
            })
              
    })

    it('Add data to JSON', done => {
        request(app)
            .post('/json-load/add')
            .send(o)           
            .end((err, res) => {
                expect(res.statusCode).to.equal(302);
                done()
            })
    })

})
